<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.panel');
    }

	public function ajaxSil(Request $req){
	    
        DB::table($req->tablo)->where('id', $req->id)->delete();
        return 'silme başarılı';

    }
}
