<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;
use App\Models\User;
use App\Address;
use Log;

class CompanyController extends Controller
{
    public function index()
    {
        Log::info('message');
        $companies = Company::orderBy('id', 'ASC')->get();
        return view('admin.companies.companies', compact('companies'));
    }


    public function createCompany($id='')
    {
        $company='';
        if($id!=''){
            $company = Company::find($id);
        }
        return view('admin.companies.company', compact('company'));
    }
    public function saveCompany($id='', Request $req)
    {
        if($id!=''){
            $company = Company::find($id);
        }else{
            $company = new Company();
        }
        $company->fill($req->all());
        if($company->save()){
            return redirect('yonetim/companies')->with('success', 'İşlem başarılı.');
        }else{
            return redirect()->back()->withError('İşlem sırasında bir hata oluştu.');
        }
    }

    public function addresses()
    {
      Log::info('message');
      $addresses = Address::orderBy('id', 'ASC')->get();
      return view('admin.addresses.addresses', compact('addresses'));
    }

     public function createAddress($id='')
    {
        $address='';
        if($id!=''){
            $address = Address::find($id);
        }
        return view('admin.addresses.address', compact('address'));
    }
    public function saveAddress($id='', Request $req)
    {
        if($id!=''){
            $book = Table::find($id);
        }else{
            $book = new Table();
        }
        $book->fill($req->all());
        if($book->save()){
            return redirect('yonetim/kitaplar')->with('success', 'İşlem başarılı.');
        }else{
            return redirect()->back()->withError('İşlem sırasında bir hata oluştu.');
        }
    }


}
