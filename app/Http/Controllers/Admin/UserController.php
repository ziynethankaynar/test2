<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Log;

class UserController extends Controller
{
    public function index()
    {
        Log::info('message');
        $users = User::orderBy('id','ASC')->get();
        return view('admin.users.users', compact('users'));
    }
    public function createUser($id)
    {
        $user = User::where('id',$id)->get();

        return view('admin.users.user', compact('user'));
    }
    public function saveUser($id, Request $req)
    {
        $user = User::find($id);

        $user->email = $req->email;
        $user->name = $req->name;
        $user->lastname = $req->lastname;

        $user->phone = $req->phone;
        $user->title = $req->title;
        if($user->save()){
            return redirect('yonetim/kullanicilar')->with('success', 'İşlem başarılı.');
        }else{
            return redirect()->back()->withError('İşlem sırasında bir hata oluştu.');
        }
    }

    public function logout(Request $req)
    {
    Auth::logout();
    return redirect('/');
    }
}
