<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
class YeniController extends Controller
{
   public function logout(Request $req)
   {
     Auth::logout();
     return redirect('/');
   }

   public function duzenle($id)
   {
      $user = User::where('id',$id)->first();
      return view('duzenle',compact('user'));
   }

}
