<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Address;
use App\Models\User;
use File;
class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $companies = Company::orderBy('name', 'ASC')->get();
      return view('home',compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();

      Company::create($input);

      $url = url($request->web);
      $html = file_get_contents($url);

      Company::where('name',$request->name)->update(['html'=>$html]);
      $companies = Company::orderBy('name', 'ASC')->get();
      return view('home',compact('companies'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $companies = Company::where('id',$id)->get();
      $users = User::where('company_id',$id)->get();
      $addresses = Address::where('company_id',$id)->get();
      return view('company',compact('companies','users','addresses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $companies = Company::where('id',$id)->first();

     return view('edit', compact('companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $company = Company::findOrFail($id);
      $input = $request->all();
      $company->fill($input)->save();

      $companies = Company::where('id',$id)->get();
      $users = User::where('company_id',$id)->get();
      $addresses = Address::where('company_id',$id)->get();
      return view('detail',compact('companies','users','addresses'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
