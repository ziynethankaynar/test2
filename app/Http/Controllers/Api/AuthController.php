<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use App\User;

class AuthController extends Controller
{
    public function signup(Request $request)
    {
        $request->validate([
           
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed'
        ]);
        $user = new User([
           
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        $user->save();
        
        $token =  $user->createToken('GZNKTP')->accessToken;
        return response()->json([
            'access_token' => $token,
            'status' => true
        ], 201);
    }
    
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = $request->user();
            $tokenResult = $user->createToken('GZNKTP');
            $token = $tokenResult->token;
            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'status' => true
            ]);
        }else{
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }
    }
  
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function verifyToken(){

    }
}