<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('yonetim')->group(function () {
    Route::get('/', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/', 'Auth\AdminLoginController@login');

    Route::middleware('auth:admin')->group(function () {
        Route::get('panel', 'Admin\DashboardController@index');

        Route::get('kullanicilar', 'Admin\UserController@index');
        Route::get('kullanici/{id?}', 'Admin\UserController@createUser');
        Route::post('kullanici/{id?}', 'Admin\UserController@saveUser');

         Route::get('companies', 'Admin\CompanyController@index');
        Route::get('company/{id?}', 'Admin\CompanyController@createCompany');
        Route::post('company/{id?}', 'Admin\CompanyController@saveCompany');

         Route::get('addresses', 'Admin\CompanyController@addresses');
        Route::get('address/{id?}', 'Admin\CompanyController@createAddress');
        Route::post('address/{id?}', 'Admin\CompanyController@saveAddress');

        Route::post('/AjaxSil', 'Admin\DashboardController@ajaxSil');
        Route::get('/cikis', 'Auth\AdminLoginController@logout')->name('logout');


    });
});
Route::get('/', function () {
    return view('welcome');

});

Auth::routes();
Route::get('/logout', 'YeniController@logout');

Route::get('duzenle/{id}', 'HomeController@duzenle');
Route::post('duzenle/{id}', 'HomeController@duzenleKayit');

Route::get('details/{id}', 'HomeController@detail');

Route::get('adres/{company_id}', 'HomeController@adres');
Route::post('adres/{company_id}', 'HomeController@adresKayit');

Route::get('page/{company_id}', 'HomeController@page');
Route::get('kayit/{id}/{user_id}', 'HomeController@kayit');

Route::get('ayril/{company_id}/{user_id}', 'HomeController@ayril');

Route::get('company/{id}/{user_id}', 'CompanyController@show');

Route::get('edit/{id}', 'CompanyController@edit');
Route::post('edit/{id}', 'CompanyController@update');

Route::get('create', 'CompanyController@create');
Route::post('create', 'CompanyController@store');

Route::get('home', 'CompanyController@index')->name('home');

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
