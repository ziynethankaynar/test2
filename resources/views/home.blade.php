@extends('layouts.app_u')

@section('content')

<br><br>
<style>

h2 {
  margin: 0 0 0.25em;
}
/* Reset Select */
select {
  -webkit-appearance: none;
  -moz-appearance: none;
  -ms-appearance: none;
  appearance: none;
  outline: 0;
  box-shadow: none;
  border: 0 !important;
  background: #2c3e50;
  background-image: none;
}
/* Remove IE arrow */
select::-ms-expand {
  display: none;
}
/* Custom Select */
.select {
  position: relative;
  display: flex;
  width: 20em;
  height: 3em;
  line-height: 3;
  background: #2c3e50;
  overflow: hidden;
  border-radius: .25em;
}
select {
  flex: 1;
  padding: 0 .5em;
  color: #fff;
  cursor: pointer;
}
/* Arrow */
.select::after {
  content: '\25BC';
  position: absolute;
  top: 0;
  right: 0;
  padding: 0 1em;
  background: #34495e;
  cursor: pointer;
  pointer-events: none;
  -webkit-transition: .25s all ease;
  -o-transition: .25s all ease;
  transition: .25s all ease;
}
/* Transition */
.select:hover::after {
  color: #f39c12;
}
.button {
    background: #2c3e50; /* Green */
  border: none;
  color: white;
  padding: 6px 8px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 12px;
  border-radius: .25em;
}
</style>

<div class="tablo_konum">
  <div>

    <div class="box-body container">
      <div class="row">
        <div class="col-sm-6">
          <table id="example2" style="">
            <thead>
              <tr>
                <th style="border:1px solid #000; font-weight:bold; color:#000; font-weight:bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ŞİRKETLER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ url('/create') }}"><button type="submit" class="button" name="create_company">Oluştur</button></a></th>

              </tr>
            </thead>
            <tbody>
              @auth
              <?php $user_id = Auth::id();
                    $user_name = Auth::user()->name;

                    ?>
              <?php $no=1; ?>
              @foreach($companies as $company)

              <tr>
                <td>&nbsp;&nbsp;<b style="color:#000;">{{$no}}</b>&nbsp;&nbsp;&nbsp;<a href="{{url('company/'.$company->id.'/'.$user_id)}}"style="color:#000;">{{$company->name}}</a></td>
              </tr>
             <?php $no=$no+1; ?>

              @endforeach
              @endauth
            </tbody>
          </table>
        </div>

          </div>
        </div>
      </div>
    </div><!-- /.box-body -->

  </div>
</div>

@endsection
