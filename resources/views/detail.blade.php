@extends('layouts.app_u')

@section('content')

<style>
.button {
    background: #2c3e50; /* Green */
  border: none;
  color: white;
  padding: 8px 25px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 13px;
  border-radius: .25em;
}
</style>
 @foreach($companies as $company)

	<section class="profile-detail">
		<div class="container">
			<div class="col-md-12">
				<div class="row">
					<div class="basic-information">
						<div class="col-md-3 col-sm-3">
						 <img src="" alt="" class="img-responsive">
						</div>
						<div class="col-md-9 col-sm-9">
							<div class="profile-content">
									<h2>{{$company->name}}<span></h2>
									<ul class="information">
                    @auth
                     <?php $user_id = Auth::id();
                           $user_name = Auth::user()->name;

                           ?>
										<li><span>Web:</span><a href="{{$company->web}}" target="_blank">{{$company->web}}</a></li>
                    <li>
                      <?php $no=1; ?>
                    @foreach($addresses as $address)
                    	<span>Adres &nbsp;&nbsp;{{$no}}&nbsp;:</span>{{$address->title}}</a><br><br>
                      <?php $no=$no+1; ?>
                      @endforeach
                      </li>
                    <li><span></span><a href="{{ url('adres/'.$company->id) }}"><button type="submit" class="button" name="kayit">Adres Ekle</button></a>
                      &nbsp;&nbsp;&nbsp;<a href="{{ url('edit/'.$company->id) }}"><button type="submit" class="button" name="edit">Düzenle</button></a>
                      	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ url('ayril/'.$company->id.'/'.$user_id) }}"><button type="submit" class="button" name="kayit">Ayrıl</button></a>&nbsp;&nbsp;&nbsp;
                    </li>
                   @endauth
									</ul>
								</div>
							</div>

				</div>
			</div>
		</div>
  </div>
	</section>

	@endforeach



	@endsection
