<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Kim Kimdir?</title>
        
        <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="crossorigin="anonymous"></script>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- All Plugin Css -->
		<link rel="stylesheet" href="{{asset ('app-assets/css/plugins.css')}}">

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:400,300">
	<link rel="stylesheet" type="text/css" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" href="dist/vanilla-select.css">
	 <link src="{{ url('js/sweetalert.min.js') }}" rel="stylesheet">
		<!-- Style & Common Css -->
		<link rel="stylesheet" href="{{asset ('app-assets/css/common.css')}}">
        <link rel="stylesheet" href="{{asset ('app-assets/css/main.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" />
    <link href="{{url('select/css/main.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset ('app-assets/plugins/jquery-flipster/dist/jquery.flipster.min.css')}}">


      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="{{url('select/js/extention/choices.js')}}"></script>


    </head>

    <body>

		<!-- Navigation Start  -->
		<nav class="navbar navbar-default navbar-sticky bootsnav">

			<div class="container">
				<!-- Start Header Navigation -->
			<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
						<i class="fa fa-bars"></i>
					</button>

                  <a href="{{url('/')}}"><img src="{{url('/images/logo.png')}}"></a>


				</div>
				<!-- End Header Navigation -->

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="navbar-menu">
					<ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
@if (Route::has('login'))
@auth
 <?php $id = Auth::id();
       $name = Auth::user()->name;

       ?>
							<li><a href="{{url('/home')}}">Anasayfa</a></li>
						  <li><a href="{{url('/details/'.$id)}}">Şirketim</a></li>

                    <li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$name}}</a>
								<ul class="dropdown-menu animated fadeOutUp" style="display: none; opacity: 1;">

									<li><a href="{{ url('duzenle/'.$id) }}">Düzenle</a></li>
                  <li>  <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        {{ __('ÇIKIŞ') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form></li>

								</ul>
							</li>



                    @else
                        <li><a href="{{url('/login')}}">Giriş</a></li>

                        @if (Route::has('register'))
                           	<li><a href="{{ route('register') }}">Üye ol</a></li>
                        @endif
                    @endauth

            @endif



						</ul>
				</div><!-- /.navbar-collapse -->
			</div>
		</nav>
		<!-- Navigation End  -->
