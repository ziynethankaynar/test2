@extends('layouts.admin')
@section('sayfacss')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css" type="text/css" rel="stylesheet" />
    <link href="{{ url('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/switchery.min.css') }}" rel="stylesheet">
@endsection
@section('title')
    Yönetim Ayarları
@endsection
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-body">
                        @if(session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        <form action="" method="post" class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">İd:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="id" class="form-control" value="{{old('id',  isset($address->id) ? $address->id : null)}}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">Şirket İd:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="tablo_no" class="form-control" value="{{old('company_id',  isset($address->company_id) ? $address->company_id : null)}}" />
                                </div>
                            </div>


							<div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">Adres:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="title" class="form-control" value="{{old('title',  isset($address->title) ? $address->title : null)}}" />
                                </div>
                            </div>


                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success">Kaydet</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
