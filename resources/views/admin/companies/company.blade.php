@extends('layouts.admin')
@section('sayfacss')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css" type="text/css" rel="stylesheet" />
    <link href="{{ url('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/switchery.min.css') }}" rel="stylesheet">
@endsection
@section('title')
    Yönetim Ayarları
@endsection
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-body">
                        @if(session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        <form action="" method="post" class="form-horizontal">
                            {{ csrf_field() }}

							<div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">Şirket Adı:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control" value="{{old('name',  isset($company->name) ? $company->name : null)}}" />
                                </div>
                            </div>

							<div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">Web</label>
                                <div class="col-sm-10">
                                    <input type="text" name="web" class="form-control" value="{{old('web',  isset($company->web) ? $company->web : null)}}" />
                                </div>
                            </div>





                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success">Kaydet</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
