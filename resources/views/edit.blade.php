@extends('layouts.app_u')

@section('content')
<div class="container">
 <div class="panel panel-default">
    <div class="panel-heading">Bilgileri Düzenle</div>

<section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-body">

                        @if(session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif


                        <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}


                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">Şirket Adı:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" class="form-control" value="{{$companies->name}}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="SiteTitle">İnternet Adresi:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="lastname" class="form-control" value="{{$companies->web}}" />
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success">Kaydet</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </section>
   </div> </div>

@endsection
