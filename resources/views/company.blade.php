@extends('layouts.app_u')

@section('content')

<style>
.button {
    background: #2c3e50; /* Green */
  border: none;
  color: white;
  padding: 8px 25px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 13px;
  border-radius: .25em;
}
</style>
 @foreach($companies as $company)

	<section class="profile-detail">
		<div class="container">
			<div class="col-md-12">
				<div class="row">
					<div class="basic-information">
						<div class="col-md-3 col-sm-3">
						 <img src="" alt="" class="img-responsive">
						</div>
						<div class="col-md-9 col-sm-9">
							<div class="profile-content">
									<h2>{{$company->name}}<span></h2>
									<ul class="information">
                    @auth
                     <?php $user_id = Auth::id();
                           $user_name = Auth::user()->name;

                           ?>
										<li><span>Web:</span><a href="{{ url('page/'.$company->id) }}" target="_blank">{{$company->web}}</a></li>
                    <li>
                      <?php $no=1; ?>
                    @foreach($addresses as $address)
                    	<span>Adres &nbsp;&nbsp;{{$no}}&nbsp;:</span>{{$address->title}}<br><br>
                      <?php $no=$no+1; ?>
                      @endforeach
                      </li>
                    <li><span></span>	<a href="{{ url('kayit/'.$company->id.'/'.$user_id) }}"><button type="submit" class="button" name="kayit">Kayıt Ol</button></a></li>
                   @endauth
									</ul>
								</div>
							</div>
              <ul class="social">
              						</ul>
            <div class="panel panel-default">
  							<div class="panel-heading">
  								<i class="fa fa-user fa-fw"></i> Kayıtlı Üyeler:
  							</div>
  												<!-- /.panel-heading -->
  							<div class="panel-body">
  							<ul>
                    @foreach($users as $user)
  							              <li>{{$user->name}}{{$user->lastname}}</li>

                    @endforeach
  							</ul>
  							</div>
  						</div>
				</div>
			</div>
		</div>
  </div>
	</section>

	@endforeach



	@endsection
